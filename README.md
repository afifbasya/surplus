# SURPLUS TEST (Muhammad Afifuddin)

Technical Test Surplus Indonesia as Mobile Developer

How to running on Android :

1. Setup react-native CLI environment (https://reactnative.dev/docs/environment-setup)
2. Run command npm install
3. Create emulator in android studio
4. Run command npx react-native start
5. Run command npx react-native run-android

How to running on ios :

1. Setup react-native CLI environment (https://reactnative.dev/docs/environment-setup)
2. Run command npm install
3. Run command cd ios
4. Run command pod install
5. Run command npx react-native start
6. Run command npx react-native run-ios
7. For spesific emulator, example : run command react-native run-ios --simulator="iPhone 14 Pro"

Command for build release android :

1. Run command react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle
2. Run command cd android
3. For windows run command gradlew assembleRelease, for unix run command ./gradlew assembleRelease
4. Result release apk nya in path android/app/build/outputs/apk/release/[nameApp].apk

Command untuk release aab :

1. sudo keytool -genkey -v -keystore baf-mobile-key.keystore -alias baf-mobile-alias -keyalg RSA -keysize 2048 -validity 10000
2. copy file keystore -> android/app

3. Add code in gradle.properties
   MYAPP_UPLOAD_STORE_FILE=surplus-mobile-key.keystore
   MYAPP_UPLOAD_KEY_ALIAS=surplus-mobile-alias
   MYAPP_UPLOAD_STORE_PASSWORD=Surplus100
   MYAPP_UPLOAD_KEY_PASSWORD=Surplus100

4. Add code in build.gradle
   signingConfigs {
   release {
   if (project.hasProperty('MYAPP_UPLOAD_STORE_FILE')) {
   storeFile file(MYAPP_UPLOAD_STORE_FILE)
   storePassword MYAPP_UPLOAD_STORE_PASSWORD
   keyAlias MYAPP_UPLOAD_KEY_ALIAS
   keyPassword MYAPP_UPLOAD_KEY_PASSWORD
   }
   }
   debug {
   storeFile file('debug.keystore')
   storePassword 'android'
   keyAlias 'androiddebugkey'
   keyPassword 'android'
   }
   }

   buildTypes {
   debug {
   signingConfig signingConfigs.debug
   }
   release {
   signingConfig signingConfigs.release
   minifyEnabled enableProguardInReleaseBuilds
   proguardFiles getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro"
   }
   }

5. ./gradlew bundleRelease
6. npx react-native run-android --variant=release
