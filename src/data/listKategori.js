import { BahanMakanan, BuahSayur, Camilan, MakananBerat, MakananVegan, Minuman, RotiKue } from "../assets";

export const listKategori = [
    {
        id: 1,
        image: MakananBerat
    },
    {
        id: 2,
        image: MakananVegan
    },
    {
        id: 3,
        image: RotiKue
    },
    {
        id: 4,
        image: Camilan
    },
    {
        id: 5,
        image: BuahSayur
    },
    {
        id: 6,
        image: Minuman
    },
    {
        id: 7,
        image: BahanMakanan
    },
]