import { ScrollView, StyleSheet } from 'react-native'
import React from 'react'
import { colors } from '../../styles'
import { Gap, Header, Kategori, RestoranTerdekat, Slider, SurplusPay, Text } from '../../components'

export default function Home() {

    return (
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
            <Header />
            <Gap height={15} />
            <Slider />
            <Gap height={20} />
            <SurplusPay />
            <Gap height={15} />
            <Kategori />
            <Gap height={30} />
            <RestoranTerdekat />
        </ScrollView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
})