import { StyleSheet, View } from 'react-native'
import React from 'react'
import { colors } from '../../styles'
import { Text } from '../../components'

export default function Pesanan() {
    return (
        <View style={styles.container}>
            <Text center>Pesanan {"\n"}Coming Soon!</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white
    }
})