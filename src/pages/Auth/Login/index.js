import { Dimensions, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { AuthImage, Google } from '../../../assets'
import { Button, Gap, InputText, Text } from '../../../components'
import { useForm } from 'react-hook-form';
import { colors } from '../../../styles';
import { useDispatch, useSelector } from 'react-redux';
import { setError, setLoading, toLogin } from '../../../services';
import Toast from 'react-native-simple-toast';

export default function Login({ navigation }) {
    const {
        control,
        handleSubmit,
        formState: { errors },
        watch,
    } = useForm({
        defaultValues: {
            email: "afifbasya@gmail.com",
            password: "rahasia"
        }
    });
    const { loading } = useSelector((state) => state.auth);
    const dispatch = useDispatch();

    const onSubmit = (data) => {
        dispatch(setLoading(true));
        setTimeout(() => {
            if (data.email === "afifbasya@gmail.com" && data.password === "rahasia") {
                dispatch(toLogin(data));
                navigation.navigate('MainApp');
            } else {
                Toast.show("Maaf Email/Password Salah", Toast.SHORT);
                dispatch(setError("Maaf Email/Password Salah"));
            }
        }, 2000)
    }

    return (
        <View style={styles.container}>
            <Image source={AuthImage} style={styles.image} />
            <View style={styles.title}>
                <Text white header3 semiBold>Masuk</Text>
                <Text white extraSmall>Pastikan kamu sudah pernah {"\n"}membuat akun Surplus</Text>
            </View>
            <View style={styles.card}>
                <InputText
                    name="email"
                    label="E-mail"
                    placeholder="Alamat email kamu . . ."
                    control={control}
                    rules={{
                        required: "Email Harus Diisi",
                        pattern: {
                            value: /\S+@\S+\.\S+/,
                            message: "Isi tidak sesuai format Email",
                        },
                    }}
                />
                <Gap height={20} />
                <InputText
                    name="password"
                    label="Kata Sandi"
                    placeholder="Masukkan Kata Sandi . . ."
                    isPassword
                    control={control}
                    rules={{ required: "Password Harus Diisi" }}
                />
                <Gap height={15} />
                <TouchableOpacity
                    onPress={() => navigation.navigate("Forgot")}
                    style={{ alignSelf: "flex-end" }}
                >
                    <Text small mute>Lupa Kata Sandi ?</Text>
                </TouchableOpacity>
                <Gap height={30} />

                <Button
                    title="Masuk"
                    onPress={handleSubmit(onSubmit)}
                    disabled={watch('email') && watch('password') ? false : true}
                    isLoading={loading}
                />
                <Gap height={20} />

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={styles.border} />
                    <View style={{ width: '20%', alignItems: 'center' }}>
                        <Text mute small>Atau</Text>
                    </View>
                    <View style={styles.border} />
                </View>
                <Gap height={20} />

                <View style={{ width: '50%', alignSelf: 'center' }}>
                    <Button
                        title="Google"
                        icon={<Google />}
                        isIcon
                    />
                </View>

                <Gap height={30} />

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Text small>Belum punya akun ?</Text>
                    <Gap width={5} />
                    <TouchableOpacity onPress={() => navigation.navigate('Register')}><Text small primary>Yuk daftar</Text></TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.secondary
    },
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 4,
        resizeMode: 'cover'
    },
    title: {
        position: 'absolute',
        top: 110,
        left: 20
    },
    card: {
        flex: 1,
        backgroundColor: colors.white,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        padding: 30,
        marginTop: -10,
    },
    border: {
        backgroundColor: colors.border,
        padding: 0.5,
        height: 0.5,
        width: '40%'
    }
})