import { Dimensions, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { AuthImage, Fb, Google } from '../../../assets'
import { Button, Gap, InputText, Text } from '../../../components'
import { useForm } from 'react-hook-form';
import { colors } from '../../../styles';
import { useDispatch, useSelector } from 'react-redux';
import { setLoading, toLogin } from '../../../services';

export default function Register({ navigation }) {
    const {
        control,
        handleSubmit,
        formState: { errors },
        watch,
    } = useForm({
        defaultValues: {
            email: "afifbasya@gmail.com",
            password: "rahasia",
            password_confirmation: "rahasia"
        }
    });
    const { loading } = useSelector((state) => state.auth);
    const dispatch = useDispatch();


    const onSubmit = (data) => {
        dispatch(setLoading(true));
        setTimeout(() => {
            dispatch(toLogin(data));
            navigation.navigate('MainApp');
        }, 2000)
    }

    return (
        <View style={styles.container}>
            <Image source={AuthImage} style={styles.image} />
            <View style={styles.title}>
                <Text white header3 semiBold>Daftar</Text>
                <Text white extraSmall>Lengkapi isian untuk mendaftar</Text>
            </View>
            <View style={styles.card}>
                <InputText
                    name="email"
                    label="E-mail"
                    placeholder="Alamat email kamu . . ."
                    control={control}
                    rules={{
                        required: "Email Harus Diisi",
                        pattern: {
                            value: /\S+@\S+\.\S+/,
                            message: "Isi tidak sesuai format Email",
                        },
                    }}
                />
                <Gap height={10} />
                <InputText
                    name="password"
                    label="Kata Sandi"
                    placeholder="Masukkan Kata Sandi . . ."
                    isPassword
                    control={control}
                    rules={{ required: "Password Harus Diisi" }}
                />
                <Gap height={10} />
                <InputText
                    name="password_confirmation"
                    label="Ulangi Kata Sandi"
                    placeholder="Ulangi Kata Sandi . . ."
                    isPassword
                    control={control}
                    rules={{
                        required: "Password Harus Diisi",
                        validate: (val) => {
                            if (watch('password') != val) {
                                return "Password dan Konfirmasi Password Tidak Sama";
                            }
                        },
                    }}
                />
                <Gap height={30} />

                <Button
                    title="Daftar"
                    onPress={handleSubmit(onSubmit)}
                    disabled={watch('email') && watch('password') && watch('password_confirmation') ? false : true}
                    isLoading={loading}
                />
                <Gap height={15} />

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={styles.border} />
                    <View style={{ width: '20%', alignItems: 'center' }}>
                        <Text mute small>Atau</Text>
                    </View>
                    <View style={styles.border} />
                </View>
                <Gap height={15} />

                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <View style={{ width: '45%' }}>
                        <Button
                            title="Facebook"
                            icon={<Fb />}
                            isIcon
                        />
                    </View>
                    <View style={{ width: '45%' }}>
                        <Button
                            title="Google"
                            icon={<Google />}
                            isIcon
                        />
                    </View>
                </View>

                <Gap height={20} />

                <Text extraSmall center mute>Dengan daftar atau masuk, Anda menerima <Text extraSmall center warning>syarat dan ketentuan</Text> serta <Text extraSmall center warning>kebijakan privasi</Text></Text>

                <Gap height={20} />

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Text small>Sudah punya akun ?</Text>
                    <Gap width={5} />
                    <TouchableOpacity onPress={() => navigation.navigate('Login')}><Text small primary>Yuk Masuk</Text></TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.secondary
    },
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 4,
        resizeMode: 'cover'
    },
    title: {
        position: 'absolute',
        top: 125,
        left: 20
    },
    card: {
        flex: 1,
        backgroundColor: colors.white,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        padding: 30,
        marginTop: -10,
    },
    border: {
        backgroundColor: colors.border,
        padding: 0.5,
        height: 0.5,
        width: '40%'
    }
})