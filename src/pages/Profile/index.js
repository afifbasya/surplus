import { StyleSheet, View } from 'react-native'
import React from 'react'
import { colors } from '../../styles'
import { Button, Gap, Text } from '../../components'
import { useDispatch, useSelector } from 'react-redux'
import { toLogout } from '../../services'

export default function Profile({ navigation }) {
    const dispatch = useDispatch();
    const { user } = useSelector((state) => state.auth);

    const handleLogout = () => {
        dispatch(toLogout());
        navigation.navigate('Boarding')
    }

    return (
        <View style={styles.container}>
            <Text center>Profile {"\n"}Coming Soon!</Text>
            <Gap height={20} />
            {user ? (
                <View style={{ width: '80%' }}>
                    <Button title="Logout" onPress={() => handleLogout()} />
                </View>
            ) : (
                <View style={{ width: '80%' }}>
                    <Button title="Login" onPress={() => navigation.navigate('Login')} />
                </View>
            )}

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
    }
})