import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect } from 'react'
import { Cert, Logo } from '../../assets'
import { colors, fonts } from '../../styles'

export default function Splash({ navigation }) {

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('Boarding')
        }, 3000)
    }, [])

    return (
        <View style={styles.container}>
            <Image source={Logo} style={styles.image} />
            <Text style={styles.text}>Save food. Save budget</Text>
            <Text style={styles.text}>Save planet</Text>
            <Image source={Cert} style={styles.cert} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    image: {
        width: 150,
        height: 150,
        resizeMode: 'contain'
    },
    cert: {
        height: 80,
        resizeMode: 'contain',
        position: 'absolute',
        bottom: 50,
    },
    text: {
        color: colors.primary,
        fontFamily: fonts.fontPoppins.medium,
        fontSize: fonts.size.medium
    }
})