import { Dimensions, Image, Platform, StyleSheet, View } from 'react-native'
import React from 'react'
import { BoardingImage } from '../../assets'
import { colors } from '../../styles'
import { Button, Gap, Text } from '../../components'

export default function Boarding({ navigation }) {
    return (
        <View style={{ flex: 1 }}>
            {Platform.OS === "ios" ? (
                <View style={styles.lewatiIOS}>
                    <Button title="Lewati" outline padding={8} border={1} onPress={() => navigation.navigate('MainApp')} />
                </View>
            ) : (
                <View style={styles.lewati}>
                    <Button title="Lewati" outline padding={8} border={1} onPress={() => navigation.navigate('MainApp')} />
                </View>
            )}

            <Image source={BoardingImage} style={styles.image} />
            <View style={styles.card}>
                <Text header4 semiBold>Selamat Datang di Surplus</Text>
                <Gap height={8} />
                <Text mute small center>Selamatkan makanan berlebih di aplikasi {"\n"} Surplus agar tidak terbuang sia-sia</Text>

                <View style={styles.wrapperBtn}>
                    <Button title="Daftar" onPress={() => navigation.navigate('Register')} />
                    <Gap height={15} />
                    <Button title="Sudah punya akun ? Masuk" outline onPress={() => navigation.navigate('Login')} />
                </View>

                <Gap height={40} />
                <Text extraSmall center mute>Dengan daftar atau masuk, Anda menerima <Text extraSmall center warning>syarat dan ketentuan</Text> serta <Text extraSmall center warning>kebijakan privasi</Text></Text>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    image: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height / 2 + 30,
        resizeMode: 'cover'
    },
    card: {
        flex: 1,
        backgroundColor: colors.white,
        borderRadius: 40,
        padding: 30,
        marginTop: -30,
        alignItems: 'center',
    },
    wrapperBtn: {
        width: '100%',
        marginTop: 30,
    },
    lewati: {
        position: 'absolute',
        top: 15,
        right: 15,
        zIndex: 1
    },
    lewatiIOS: {
        position: 'absolute',
        top: 60,
        right: 15,
        zIndex: 1
    }
})