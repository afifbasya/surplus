const fonts = {
    fontFamily: {
        light: "Inter-Light",
        regular: "Inter-Regular",
        semiBold: "Inter-SemiBold",
        bold: "Inter-Bold",
    },
    fontPoppins: {
        light: "Poppins-Light",
        regular: "Poppins-Regular",
        semiBold: "Poppins-SemiBold",
        bold: "Poppins-Bold",
        medium: "Poppins-Medium",
    },
    size: {
        header1: 36,
        header2: 32,
        header3: 24,
        header4: 20,
        normal: 15,
        medium: 17,
        small: 13,
        extraSmall: 12,
    },
};

export default fonts;
