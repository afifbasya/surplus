const colors = {
  primary: "#009688",
  secondary: '#31AA8B',
  white: "#FFFFFF",
  dark: '#424242',
  yellow: '#F3AF28',
  border: "#D9D9D9",
  border2: '#B5B5B5',
  border3: '#F4F4F4'
};

export default colors;
