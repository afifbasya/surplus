import { Image, ScrollView, StyleSheet, View } from 'react-native'
import React from 'react'
import { Gap, Text } from '../../atoms'
import { listKategori } from '../../../data'

export default function Kategori() {
    return (
        <View style={styles.container}>
            <Text semiBold>Kategori</Text>
            <Gap height={10} />
            <View style={styles.kategori}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    {listKategori.map((kategori, index) => {
                        return <Image key={index} source={kategori.image} style={styles.image} />
                    })}
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 15
    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 8,
        marginRight: 8
    },
    kategori: {
        flexDirection: 'row'
    }
})