import { ActivityIndicator, Image, ScrollView, StyleSheet, View } from 'react-native'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getPhotos } from '../../../services';
import { Gap, Text } from '../../atoms';
import { colors } from '../../../styles';

export default function RestoranTerdekat() {
    const dispatch = useDispatch();
    const { loading, restorans } = useSelector((state) => state.common);


    useEffect(() => {
        dispatch(getPhotos())
    }, [])

    return (
        <View style={styles.container}>
            <Text semiBold>Restoran terdekat</Text>
            <Gap height={10} />
            {loading && (
                <ActivityIndicator size="large" color={colors.primary} />
            )}
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {restorans && restorans?.map((restoran, index) => {
                    return (
                        <View key={index} style={styles.card}>
                            <Image source={{ uri: restoran.thumbnailUrl }} style={styles.image} />
                            <Text small>{restoran.title.substring(0, 10)}</Text>
                        </View>
                    )
                })}
            </ScrollView>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 15
    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 8,
        marginRight: 8
    },
    card: {
        alignItems: 'center'
    }
})