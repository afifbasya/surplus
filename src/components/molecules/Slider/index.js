import { Dimensions, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Slide1, Slide2 } from '../../../assets'
import { SliderBox } from 'react-native-image-slider-box';
import { colors } from '../../../styles';

export default function Slider() {
    return (
        <SliderBox
            images={[Slide1, Slide2, Slide1, Slide2]}
            autoplay
            circleLoop
            sliderBoxHeight={132}
            ImageComponentStyle={styles.slider}
            dotStyle={styles.dotStyle}
            dotColor={colors.primary}
            imageLoadingColor={colors.primary}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: -15,
    },
    slider: {
        borderRadius: 10,
        width: Dimensions.get('window').width - 30
    },
    dotStyle: {
        width: 10,
        height: 5,
        borderRadius: 5,
    }
})