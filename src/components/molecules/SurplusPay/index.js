import { Image, StyleSheet, View } from 'react-native'
import React from 'react'
import { colors } from '../../../styles'
import { Gap, Text } from '../../atoms'
import { Coin, Voucher } from '../../../assets'

export default function SurplusPay() {
    return (
        <View style={styles.container}>
            <View style={styles.section}>
                <Image source={Coin} style={styles.image} />
                <Gap width={5} />
                <View>
                    <Text small semiBold primary>Rp0</Text>
                    <Text small mute>SurplusPay</Text>
                </View>
            </View>
            <View style={styles.border} />
            <View style={styles.section}>
                <Image source={Voucher} style={styles.image2} />
                <Gap width={5} />
                <Text small semiBold primary>5 voucher tersedia</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 15,
        backgroundColor: colors.border3,
        paddingVertical: 12,
        paddingHorizontal: 20,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    image: {
        width: 45,
        height: 45
    },
    image2: {
        width: 30,
        height: 30
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    border: {
        padding: 1.5,
        backgroundColor: '#C4C4C4',
        marginVertical: 2
    }
})