import Slider from './Slider'
import SurplusPay from './SurplusPay'
import Kategori from './Kategori'
import RestoranTerdekat from './RestoranTerdekat'
import Header from './Header'

export { Header, RestoranTerdekat, Kategori, Slider, SurplusPay }