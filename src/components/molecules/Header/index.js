import { Image, Platform, StyleSheet, View } from 'react-native'
import React, { useEffect } from 'react'
import { ArrowDown, Cart } from '../../../assets'
import { Gap, InputText, Text } from '../../atoms'
import { colors } from '../../../styles'
import { useDispatch, useSelector } from 'react-redux'
import { getWeather } from '../../../services'

export default function Header() {
    const dispatch = useDispatch();
    const { weather } = useSelector((state) => state.common);
    const { user } = useSelector((state) => state.auth);

    useEffect(() => {
        dispatch(getWeather());
    }, [])

    return (
        <View>
            <View style={styles.wrapperHeader}>
                {Platform.OS == "ios" && <Gap height={50} />}
                <View style={styles.header}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image source={{ uri: `https:${weather?.current?.condition?.icon}` }} style={styles.icon} />
                        <View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text white extraSmall>Lokasi Kamu</Text>
                                <ArrowDown />
                            </View>
                            <Text white semiBold>{weather?.location?.name}, {weather?.location?.region}</Text>
                        </View>
                    </View>
                    <Cart />
                </View>
                <Gap height={20} />
                <Text white semiBold header4>Hi, {user ? user.email : "Surplus Hero"}</Text>
                <Gap height={15} />
            </View>

            <View style={styles.search}>
                <InputText
                    placeholder="Mau selamatkan makanan apa hari ini ?"
                    isSearch
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    wrapperHeader: {
        backgroundColor: colors.primary,
        padding: 15,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
    },
    search: {
        paddingHorizontal: 15,
        marginTop: -20
    },
    icon: {
        width: 50,
        height: 50
    }
})