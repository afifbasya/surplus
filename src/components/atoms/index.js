import Button from './Button'
import Gap from './Gap'
import Text from './Text'
import InputText from './InputText'
import MyTab from './MyTab'

export { MyTab, InputText, Button, Gap, Text }
