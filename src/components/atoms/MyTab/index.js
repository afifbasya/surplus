import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { colors, fonts } from '../../../styles';
import { Discover, DiscoverActive, Forum, ForumActive, Pesanan, PesananActive, Profile, ProfileActive } from '../../../assets';

function MyTabBar({ state, descriptors, navigation }) {
    return (
        <View style={styles.container}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        // The `merge: true` option makes sure that the params inside the tab screen are preserved
                        navigation.navigate({ name: route.name, merge: true });
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                const Icon = () => {
                    if (label === "Discover") {
                        return isFocused ? <DiscoverActive /> : <Discover />;
                    }

                    if (label === "Pesanan") {
                        return isFocused ? <PesananActive /> : <Pesanan />;
                    }

                    if (label === "Forum") {
                        return isFocused ? <ForumActive /> : <Forum />;
                    }

                    if (label === "Profile") {
                        return isFocused ? <ProfileActive /> : <Profile />;
                    }

                    return <DiscoverActive />;
                };

                return (
                    <TouchableOpacity
                        key={index}
                        accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={styles.tab}
                    >
                        <Icon />
                        <Text style={styles.text(isFocused)}>
                            {label}
                        </Text>
                    </TouchableOpacity>
                );
            })}
        </View>
    );
}

export default MyTabBar;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        paddingHorizontal: 15,
        paddingVertical: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        flexDirection: "row",
        alignItems: "center",
    },
    text: (isFocused) => ({
        color: isFocused ? colors.primary : colors.border2,
        textTransform: 'uppercase',
        fontFamily: fonts.fontFamily.semiBold,
        fontSize: 12
    }),
    tab: {
        flex: 1,
        alignItems: "center",
    }
});
