import {
  ActivityIndicator,
  View,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import React from "react";
import { colors } from "../../../styles";
import Text from "../Text";
import Gap from "../Gap";
import { Google } from "../../../assets";

export default function Button({ isIcon, icon, title, onPress, isLoading, outline, padding = 14, border = 2, disabled }) {
  if (isLoading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator size="small" color={colors.white} />
        <Gap width={5} />
        <Text white semiBold>
          {title}
        </Text>
      </View>
    );
  }

  if (disabled) {
    return (
      <View style={styles.loading}>
        <Gap width={5} />
        <Text white semiBold>
          {title}
        </Text>
      </View>
    );
  }

  if (outline) {
    return (
      <TouchableOpacity style={styles.containerOutline(padding, border)} onPress={onPress}>
        <Text primary semiBold>
          {title}
        </Text>
      </TouchableOpacity>
    );
  }

  if (icon) {
    return (
      <TouchableOpacity style={styles.loading} onPress={onPress}>
        {icon}
        <Gap width={10} />
        <Text semiBold>
          {title}
        </Text>
      </TouchableOpacity>
    )
  }

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text white semiBold>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    padding: 14,
    alignItems: "center",
    borderRadius: 25,
  },
  containerOutline: (padding, border) => ({
    padding: padding,
    alignItems: "center",
    borderRadius: 25,
    borderWidth: border,
    borderColor: colors.primary
  }),
  loading: {
    backgroundColor: colors.border,
    padding: 14,
    alignItems: "center",
    borderRadius: 25,
    flexDirection: "row",
    justifyContent: "center",
  },
});
