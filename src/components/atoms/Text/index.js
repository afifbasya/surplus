import React from "react";
import { Text as Txt } from "react-native";
import { colors, fonts } from "../../../styles";

export default (props) => {
  const fontSize = fonts.size;
  const fontFamily = fonts.fontFamily;

  let styles = {
    color: colors.dark,
    fontSize: fontSize.normal,
    fontFamily: fontFamily.regular,
    textAlign: 'left'
  };

  //Font Family
  if (props.light) styles.fontFamily = fontFamily.light;
  else if (props.regular) styles.fontFamily = fontFamily.regular;
  else if (props.semiBold) styles.fontFamily = fontFamily.semiBold;
  else if (props.bold) styles.fontFamily = fontFamily.bold;

  //Font Size
  if (props.header1) styles.fontSize = fontSize.header1;
  else if (props.header2) styles.fontSize = fontSize.header2;
  else if (props.header3) styles.fontSize = fontSize.header3;
  else if (props.header4) styles.fontSize = fontSize.header4;
  else if (props.normal) styles.fontSize = fontSize.normal;
  else if (props.small) styles.fontSize = fontSize.small;
  else if (props.extraSmall) styles.fontSize = fontSize.extraSmall;

  //Font Color
  if (props.primary) styles.color = colors.primary;
  else if (props.secondary) styles.color = colors.secondary;
  else if (props.success) styles.color = colors.success;
  else if (props.danger) styles.color = colors.danger;
  else if (props.mute) styles.color = colors.mute;
  else if (props.white) styles.color = colors.white;
  else if (props.warning) styles.color = colors.yellow;

  if (props.center) styles.textAlign = "center";

  return (
    <Txt style={[props.style, styles]} {...props}>
      {props.children}
    </Txt>
  );
};
