import { StyleSheet, View, TextInput } from "react-native";
import React from "react";
import Text from "../Text";
import { colors, fonts } from "../../../styles";
import Gap from "../Gap";
import InputNoHp from "./InputNoHp";
import InputPassword from "./InputPassword";
import InputSearch from "./InputSearch";
import { Controller } from "react-hook-form";

export default function InputText(props) {
  const {
    label,
    placeholder,
    isPassword,
    isHandphone,
    isSearch,
    control,
    rules = {},
    name = "",
  } = props;

  if (isSearch) {
    return <InputSearch {...props} />;
  }

  if (isHandphone) {
    return <InputNoHp {...props} />;
  }

  if (isPassword) {
    return <InputPassword {...props} />;
  }

  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({
        field: { value, onChange, onBlur },
        fieldState: { error },
      }) => {
        return (
          <>
            <Text semiBold danger={error}>{label}</Text>
            <Gap height={6} />
            <TextInput
              value={value}
              onChangeText={onChange}
              onBlur={onBlur}
              placeholder={placeholder}
              placeholderTextColor={colors.mute}
              cursorColor={colors.primary}
              style={styles.input(error)}
            />
            {error && (
              <Text style={styles.error}>{error.message || "Error"}</Text>
            )}
          </>
        );
      }}
    />
  );
}

const styles = StyleSheet.create({
  input: (error) => ({
    borderWidth: 2,
    borderColor: error ? colors.danger : colors.border,
    paddingHorizontal: 14,
    paddingVertical: 10,
    borderRadius: 8,
    fontFamily: fonts.fontFamily.light,
    fontSize: 15,
    width: "100%",
  }),
  error: { color: colors.danger, alignSelf: "stretch" },
});
