import { StyleSheet, View, TextInput, TouchableOpacity, Platform } from "react-native";
import React, { useState } from "react";
import Text from "../Text";
import { colors, fonts } from "../../../styles";
import Gap from "../Gap";
import { IconSearch } from "../../../assets";

export default function InputSearch({
  label,
  onChangeText,
  onEndEditing,
  value,
  placeholder,
}) {
  return (
    <View>
      {/* <Text>{label}</Text>
      <Gap height={8} />
      <View> */}
      <View style={styles.search}>
        <IconSearch color={colors.border2} />
      </View>
      <TextInput
        style={styles.inputHP}
        onChangeText={onChangeText}
        value={value}
        placeholder={placeholder}
        placeholderTextColor={colors.mute}
        cursorColor={colors.primary}
        onEndEditing={onEndEditing}
      />
      {/* </View> */}
    </View>
  );
}

const styles = StyleSheet.create({
  search: {
    position: "absolute",
    margin: Platform.OS === "android" ? 15 : 10,
    zIndex: 1,
    right: 0
  },
  inputHP: {
    borderWidth: 1,
    borderColor: colors.border,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 10,
    fontFamily: fonts.fontFamily.light,
    backgroundColor: colors.white,
    fontSize: 15,
  },
});
