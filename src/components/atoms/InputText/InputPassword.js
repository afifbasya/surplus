import { StyleSheet, View, TextInput, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import Text from "../Text";
import { colors, fonts } from "../../../styles";
import Gap from "../Gap";
import { EyeOff, Eye } from "../../../assets";
import { Controller } from "react-hook-form";

export default function InputPassword({
  label,
  placeholder,
  control,
  rules = {},
  name = "",
}) {
  const [show, setShow] = useState(true);

  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({
        field: { value, onChange, onBlur },
        fieldState: { error },
      }) => {
        return (
          <>
            <Text semiBold danger={error}>{label}</Text>
            <Gap height={6} />
            <View style={styles.wrapperPass}>
              <TextInput
                value={value}
                onChangeText={onChange}
                onBlur={onBlur}
                placeholder={placeholder}
                placeholderTextColor={colors.mute}
                cursorColor={colors.primary}
                style={styles.input(error)}
                secureTextEntry={show}
              />
              <TouchableOpacity
                style={styles.eye}
                onPress={() => setShow(!show)}
              >
                {show ? <Eye /> : <EyeOff />}
              </TouchableOpacity>
            </View>

            {error && (
              <Text style={styles.error}>{error.message || "Error"}</Text>
            )}
          </>
        );
      }}
    />
  );
}

const styles = StyleSheet.create({
  input: (error) => ({
    borderWidth: 2,
    borderColor: error ? colors.danger : colors.border,
    paddingHorizontal: 14,
    paddingVertical: 10,
    borderRadius: 8,
    fontFamily: fonts.fontFamily.light,
    fontSize: 15,
    width: "100%",
  }),
  wrapperPass: {
    flexDirection: "row",
    alignItems: "center",
  },
  eye: {
    right: 40,
  },
  error: { color: colors.danger, alignSelf: "stretch" },
});
