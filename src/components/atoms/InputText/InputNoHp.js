import { StyleSheet, View, TextInput } from "react-native";
import React from "react";
import Text from "../Text";
import { colors, fonts } from "../../../styles";
import Gap from "../Gap";
import { Controller } from "react-hook-form";

export default function InputNoHp({
  label,
  placeholder,
  control,
  rules = {},
  name = "",
}) {
  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({
        field: { value, onChange, onBlur },
        fieldState: { error },
      }) => {
        return (
          <>
            <Text danger={error}>{label}</Text>
            <Gap height={8} />
            <View>
              <View style={styles.hp}>
                <Text light>+62</Text>
              </View>
              <TextInput
                value={value}
                onChangeText={onChange}
                onBlur={onBlur}
                placeholder={placeholder}
                placeholderTextColor={colors.mute}
                cursorColor={colors.primary}
                style={styles.inputHP(error)}
                keyboardType="number-pad"
              />
            </View>
            {error && (
              <Text style={styles.error}>{error.message || "Error"}</Text>
            )}
          </>
        );
      }}
    />
  );
}

const styles = StyleSheet.create({
  hp: {
    position: "absolute",
    top: 14,
    left: 10,
    borderRightWidth: 1,
    paddingRight: 8,
    borderColor: colors.border,
  },
  inputHP: (error) => ({
    borderWidth: 1,
    borderColor: error ? colors.danger : colors.border,
    paddingHorizontal: 55,
    paddingVertical: 10,
    borderRadius: 5,
    fontFamily: fonts.fontFamily.light,
    fontSize: 15,
  }),
  error: { color: colors.danger, alignSelf: "stretch" },
});
