import React from "react";
import Svg, { Path } from "react-native-svg";

export default function ArrowRight() {
  return (
    <Svg
      width="30"
      height="30"
      viewBox="0 0 30 30"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <Path
        d="M9.9875 16.25H23.75C24.4375 16.25 25 15.6875 25 15C25 14.3125 24.4375 13.75 23.75 13.75L9.9875 13.75L9.9875 11.5125C9.9875 10.95 9.3125 10.675 8.925 11.075L5.45 14.5625C5.2125 14.8125 5.2125 15.2 5.45 15.45L8.925 18.9375C9.3125 19.3375 9.9875 19.05 9.9875 18.5V16.25Z"
        fill="#3640B7"
      />
    </Svg>
  );
}
