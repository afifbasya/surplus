import Logo from './logo.png'
import Cert from './cert.png'
import BoardingImage from './boarding.png'
import AuthImage from './auth.png'
import Slide1 from './slide1.png'
import Slide2 from './slide2.png'
import Coin from './coin.png'
import Voucher from './voucher.png'
import BuahSayur from './BuahSayur.webp'
import Camilan from './Camilan.webp'
import MakananBerat from './MakananBerat.webp'
import MakananVegan from './MakananVegan.webp'
import Minuman from './Minuman.webp'
import RotiKue from './RotiKue.webp'
import BahanMakanan from './BahanMakanan.webp'


export { Camilan, Voucher, Coin, Logo, Cert, BoardingImage, AuthImage, Slide1, Slide2, BuahSayur, MakananBerat, MakananVegan, Minuman, RotiKue, BahanMakanan }