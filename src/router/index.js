import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Forum, Home, Login, Pesanan, Profile, Register, Splash, Boarding } from '../pages'
import { MyTab } from '../components';

const Tab = createBottomTabNavigator();

function MainApp() {
    return (
        <Tab.Navigator tabBar={props => <MyTab {...props} />}>
            <Tab.Screen name="Discover" component={Home} options={{ headerShown: false }} />
            <Tab.Screen name="Pesanan" component={Pesanan} />
            <Tab.Screen name="Forum" component={Forum} />
            <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    );
}

const Stack = createNativeStackNavigator();

function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
                <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }} />
                <Stack.Screen name="Boarding" component={Boarding} options={{ headerShown: false }} />
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Router;