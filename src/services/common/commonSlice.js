import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    loading: false,
    restorans: false,
    weather: false,
    error: "",
};

export const getPhotos = createAsyncThunk("restoran", () => {
    return axios.get(`https://jsonplaceholder.typicode.com/photos?_limit=10`).then((response) => response.data);
});

export const getWeather = createAsyncThunk("weather", () => {
    return axios.get(`https://api.weatherapi.com/v1/current.json?key=66ae1f59001241aabfc43332230806&q=Pekalongan&aqi=no`).then((response) => response.data);
});

const commonSlice = createSlice({
    name: "common",
    initialState,
    extraReducers: (builder) => {
        // GET PHOTO JSONPLACEHOLDER
        builder.addCase(getPhotos.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getPhotos.fulfilled, (state, action) => {
            state.loading = false;
            state.restorans = action.payload;
            state.error = "";
        });
        builder.addCase(getPhotos.rejected, (state, action) => {
            state.loading = false;
            state.restorans = false;
            state.error = action.error.message;
        });

        // GET WEATHER API
        builder.addCase(getWeather.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(getWeather.fulfilled, (state, action) => {
            state.loading = false;
            state.weather = action.payload;
            state.error = "";
        });
        builder.addCase(getWeather.rejected, (state, action) => {
            state.loading = false;
            state.weather = false;
            state.error = action.error.message;
        });
    },
});

export default commonSlice.reducer;
