import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loading: false,
  user: false,
  error: "",
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
      state.loading = false;
    },
    toLogin: (state, action) => {
      state.user = action.payload;
      state.loading = false;
    },
    toLogout: (state, action) => {
      state.user = false;
    },
  },
  extraReducers: (builder) => {
  },
});

export default authSlice.reducer;
export const { toLogin, setError, setLoading, toLogout } = authSlice.actions;
